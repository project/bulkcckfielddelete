
This module helps to delete more number of CCK fields from more than one content type at a time. User can delete "N" number of unwanted managed fields/CCKfields at a time. This module provide a path where all the content types and their associated CCK fields are displayed. User can select and delete the fields one shot.

Features:
---------

Delete more number of CCK fields from more than one content type at a time.

Installation:
------------

Copy bulkcckfielddelete.module to your module directory and then enable on the admin
modules page.

Link to the bulk cck field delete page:
---------------------------------------

admin/config/user-interface/cck_bulk_field_delete